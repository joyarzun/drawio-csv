const {
  createLink,
  bytesToString,
  encodeDeflateBase64
} = require("../src/createLink");

describe("createLink", () => {
  it("returns a link with drawio domain", () => {
    expect(createLink()).toMatch("https://www.draw.io");
  });

  it("includes title when config have it", () => {
    expect(createLink({ title: "test" })).toMatch(
      "https://www.draw.io?title=test"
    );
  });

  it("encodes parameters", () => {
    expect(createLink({ title: "?x=test" })).toMatch(
      "https://www.draw.io?title=%3Fx%3Dtest"
    );
  });

  it("has #S paramete", () => {
    expect(
      createLink(
        { title: "?x=test" },
        { url: "myurl", format: "csv", data: "test" }
      )
    ).toMatch("#S");
  });
});

describe("bytesToString", () => {
  it("converts byte to strings", () => {
    expect(bytesToString([70, 70])).toBe("FF");
  });
});

describe("encodeDeflateBase64", () => {
  it("encodes, deflate and converts to base64", () => {
    expect(encodeDeflateBase64("test")).toBe("MDAwMDAw");
  });
});
