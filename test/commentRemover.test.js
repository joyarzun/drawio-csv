const fs = require("fs");
const path = require("path");
const commentRemover = require("../src/commentRemover");
const sampleFile = path.resolve(__dirname, "commentRemover.sample.txt");
const expectedFile = path.resolve(__dirname, "commentRemover.expected.txt");

describe("commentRemover", () => {
  it("removes comments", () => {
    expect(commentRemover(fs.readFileSync(sampleFile, "utf8"))).toBe(
      fs.readFileSync(expectedFile, "utf8")
    );
  });
});
