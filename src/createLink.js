const querystring = require("querystring");
const drawioDomain = "https://www.draw.io";
const pako = require("pako");

const bytesToString = arrBytes =>
  arrBytes.map(code => String.fromCharCode(code)).join("");

const encodeDeflateBase64 = data =>
  Buffer.from(
    bytesToString(pako.deflateRaw(encodeURIComponent(data)))
  ).toString("base64");

const createLink = (params, encodedData) => {
  const link = params
    ? [drawioDomain, querystring.stringify(params)].join("?")
    : drawioDomain;

  const url = encodedData ? encodedData.url : "";
  const format = encodedData ? encodedData.format : "";
  const data = encodedData ? encodedData.data : "";

  return `${link}#S${encodeDeflateBase64(
    JSON.stringify({ url, format, data })
  )}`;
};

module.exports = { createLink, bytesToString, encodeDeflateBase64 };
