module.exports = fileString => {
  const fileArray = fileString.split("\n");
  const regexCommentedLine = /^ *#.*$/;
  return fileArray
    .filter(line => !regexCommentedLine.test(line) && line.length > 0)
    .join("\n");
};
